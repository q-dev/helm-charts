# The Q Charts for Kubernetes

This repository contains the official provided helm charts to deploy Q related infrastructure into kubernetes clusters.

## TL;DR;

```bash
helm repo add Q https://gitlab.com/api/v4/projects/38390705/packages/helm/stable
helm install my-release Q/<chart-name>
```

## Getting started

### Prerequisites

- Kubernetes 1.18+
- Helm 3
- PV storage provisioner

### Install Helm

Helm is a tool for managing infrastructure/application charts for Kubernetes. 
Charts are packages of pre-configured Kubernetes resources.

To install Helm, refer to the [Official Helm install guide](https://helm.sh/docs/intro/install/) and ensure that the
`helm` binary is in the `PATH` variable of your shell.

### Using Helm

Once you have installed the Helm client, you can deploy a charts located in this repository into a Kubernetes cluster.

Please refer to the [Official Quick Start guide](https://helm.sh/docs/intro/quickstart/) or the
[Official Using Helm Guide](https://helm.sh/docs/intro/using_helm/) with detailed instructions on how to use the Helm 
client to manage packages on your Kubernetes cluster.

Useful Helm Client Commands:

* Install a chart: `helm install my-release Q/<chart-name>`
* Upgrade or install Q applications: `helm upgrade --install my-release Q/<chart-name>`

To start using **omnibridge-oracle-new**, **q-explorer** or **safe-transaction-svc** charts you have to download depending packages for the release.
```bash
helm package -u ./path-to-chart
```
After that, you can install your release. 